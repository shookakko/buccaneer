(defproject buccaneer "0.1.0-SNAPSHOT"
  :description ""
  :url ""
  :license {:name "" :url ""}

  :plugins [[lein-ring "0.12.6"]
            [lein-environ "1.1.0"]
            [lein-cljsbuild "1.1.7"]
            [lein-asset-minifier "0.4.6"
             :exclusions [org.clojure/clojure]]]

  :ring {:handler buccaneer.api/app
         :uberwar-name "buccaneer.war"}

  :dependencies [[org.clojure/clojure "1.10.0"]
                 [clj-commons/clj-yaml "0.7.108"]
                 [liberator "0.15.3"]
                 [compojure "1.7.0"]
                 [ring "1.9.5"]
                 [reagent "1.1.1"]
                 [reagent-utils "0.3.4"]
                 [ring/ring-defaults "0.3.3"]
                 [hiccup "1.0.5"]
                 [yogthos/config "1.2.0"]
                 [org.clojure/clojurescript "1.11.54"
                  :scope "provided"]
                 [metosin/reitit "0.5.18"]
                 [pez/clerk "1.0.0"]
                 [venantius/accountant "0.2.5"
                  :exclusions [org.clojure/tools.reader]]
                 [cheshire "5.11.0"]
                 [enlive "1.1.6"]
                 [uri "1.1.0"]
                 [clj-http "3.12.3"]]

  :jvm-opts ["-Xmx1G"]

  :min-lein-version "2.5.0"
  :uberjar-name "buccaneer.jar"
  ;; :main buccaneer.core
  :clean-targets ^{:protect false}
  [:target-path
   [:cljsbuild :builds :app :compiler :output-dir]
   [:cljsbuild :builds :app :compiler :output-to]]

  :source-paths ["src/clj" "src/cljc" "src/cljs" "src/clj/buccaneer/trackers"]
  :resource-paths ["resources" "target/cljsbuild"]

  :minify-assets
  [[:css {:source "resources/public/css/site.css"
          :target "resources/public/css/site.min.css"}]]

  :cljsbuild
  {:builds {:min
            {:source-paths ["src/cljs" "src/cljc" "env/prod/cljs"]
             :compiler
             {:output-to        "target/cljsbuild/public/js/app.js"
              :output-dir       "target/cljsbuild/public/js"
              :source-map       "target/cljsbuild/public/js/app.js.map"
              :optimizations :advanced
              :infer-externs true
              :pretty-print  false}}
            :app
            {:source-paths ["src/cljs" "src/cljc" "env/dev/cljs"]
             :figwheel {:on-jsload "buccaneer.core/mount-root"}
             :compiler
             {:main "buccaneer.dev"
              :asset-path "/js/out"
              :output-to "target/cljsbuild/public/js/app.js"
              :output-dir "target/cljsbuild/public/js/out"
              :source-map true
              :optimizations :none
              :pretty-print  true}}
            }
   }

  :profiles {:dev {:repl-options {:init-ns buccaneer.core}
                   :dependencies [[cider/piggieback "0.5.3"]
                                  [binaryage/devtools "1.0.6"]
                                  [ring/ring-mock "0.4.0"]
                                  [ring/ring-devel "1.9.5"]
                                  [prone "2021-04-23"]
                                  [figwheel-sidecar "0.5.20"]
                                  [nrepl "0.9.0"]
                                  [thheller/shadow-cljs "2.16.7"]
                                  [pjstadig/humane-test-output "0.11.0"]]
                   :source-paths ["env/dev/clj"]
                   :plugins [[lein-figwheel "0.5.20"]]
                   :injections [(require 'pjstadig.humane-test-output)
                                (pjstadig.humane-test-output/activate!)]

                   :env {:dev true}}
             :shadow-cljs {:dependencies [[com.google.javascript/closure-compiler-unshaded "v20211201"]]}
             :uberjar {:hooks [minify-assets.plugin/hooks]
                       :source-paths ["env/prod/clj"]
                       :prep-tasks ["compile" ["cljsbuild" "once" "min"]]
                       :env {:production true}
                       :aot :all
                       :omit-source true}}

  :repl-options {:init-ns buccaneer.core})

