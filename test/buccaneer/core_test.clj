(ns buccaneer.core-test
  (:require [clojure.test :refer :all]
            [buccaneer.core :refer :all]
            [buccaneer.tracker  :refer :all]))

(deftest test-1337x-search-link
  (let [results {"patata" "https://1337x.to/search/patata/"
                 "Breaking Bad" "https://1337x.to/search/Breaking+Bad/"
                 "1337 bad" "https://1337x.to/search/1337+bad/"
                 "55pa%" "https://1337x.to/search/55pa%25/"}]
    (->> results
         (every? (fn [x]
                   (testing ""
                     (is (= (last x)
                            (.toString
                             (buccaneer.tracker/generate-search-link
                              (:1337x tracker) (first x)))))))))))

(deftest test-1337x-get-page
  (testing "Test only one page"
    (is
     (-> (get-page (:1337x trackers) "patata" 1)
         (buccaneer.tracker.1337x/last-page)
         (nil?))))

  (testing "Test more than one page"
    (is
     (-> (get-page (:1337x trackers) "car" 1)
         (buccaneer.tracker.1337x/last-page)
         (buccaneer.tracker.1337x/total-pages)
         (number?)))))


(deftest test-1337x-search
  (testing ""
    (is
     (= (-> (get-page (:1337x trackers) "one punch man" 1)
            (buccaneer.tracker.1337x/last-page)
            (buccaneer.tracker.1337x/total-pages))
        (count (search (:1337x trackers) "one punch man"))))
    (is
     (= nil
        (search (:1337x trackers) "sdjh3k34jhdkfjh")))
    (is
     (= (-> (get-page (:1337x trackers) "1080" 1)
            (buccaneer.tracker.1337x/last-page)
            (buccaneer.tracker.1337x/total-pages))
        (count (search (:1337x trackers) "1080"))))))

(deftest test-freshmeat-search-link
  (let [results {"patata" "https://freshmeat.io/s?q=patata"
                 "Breaking Bad" "https://freshmeat.io/s?q=Breaking+Bad"
                 "1337 bad" "https://freshmeat.io/s?q=1337+bad"
                 "55pa%" "https://freshmeat.io/s?q=55pa%25"}]
    (->> results
         (every? (fn [x]
                   (testing ""
                     (is (= (last x)
                            (.toString
                             (buccaneer.tracker/generate-search-link
                              (:freshmeat trackers) (first x)))))))))))

(deftest test-freshmeat-get-page
  (testing "Test only one page"
    (is
     (-> (get-page (:freshmeat trackers) "patata" 0)
         (buccaneer.tracker.freshmeat/search-results)
         (buccaneer.tracker.freshmeat/total-pages)
         (= 0))))

  (testing "Test more than one page"
    (is
     (-> (get-page (:freshmeat trackers) "car" 0)
         (buccaneer.tracker.freshmeat/search-results)
         (buccaneer.tracker.freshmeat/total-pages)
         (> 0)))))

(deftest test-freshmeat-search
  (testing ""
    (is
     (= (-> (get-page (:freshmeat trackers) "one punch man" 0)
            (buccaneer.tracker.freshmeat/search-results)
            (buccaneer.tracker.freshmeat/total-pages)
            (inc))
        (count (search (:freshmeat trackers) "one punch man"))))
    (is
     (= nil
        (search (:freshmeat trackers) "sdjh3k34jhdkfjh")))
    (is
     (= (-> (get-page (:freshmeat trackers) "1080" 0)
            (buccaneer.tracker.freshmeat/search-results)
            (buccaneer.tracker.freshmeat/total-pages)
            (inc))
        (count (search (:freshmeat trackers) "1080"))))))

(deftest test-freshmeat-get-torrent-info
  (testing "Getting info from a torrent"
    (is
     (= (get-torrent-info (:freshmeat trackers) "/t/8546132")
        {:magnet "magnet:?xt=urn:btih:0110c55c36b7a3fbb40c7aa48368668019bf6b43&dn=Terminator freshmeat.io&tr=udp://tracker.opentrackr.org:1337/announce&tr=udp://tracker.zer0day.to:1337/announce&tr=udp://tracker.coppersurfer.tk:6969/announce&tr=udp://tracker.leechers-paradise.org:6969/announce&tr=udp://tracker.internetwarriors.net:1337/announce&tr=udp://mgtracker.org:6969/announce&tr=udp://explodie.org:6969/announce"
         :size "9.2 GB"
         :files 6
         :seeds nil
         :leechers nil})))
  (testing "Getting nil from an unexisting page"
    (is (nil? (get-torrent-info (:freshmeat trackers) "/t/abc")))
    (is (nil? (get-torrent-info (:freshmeat trackers) "")))))
