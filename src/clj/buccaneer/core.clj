(ns buccaneer.core
  (:require [buccaneer.tracker :as tracker]
            [clojure.java.io :as io]
            [net.cgrand.enlive-html :as enlive]
            [clojure.string :as str]
            [clj-http.client :as client]
            [buccaneer.tracker :as tracker])
  (:import (java.net URI URLEncoder URLDecoder)))

(def trackers
  {:1337x     (struct tracker/tracker
                      :1337x
                      "https://1337x.to/"
                      "1337X is a Public torrent site that offers verified torrent downloads"
                      "en-US")
   :freshmeat (struct tracker/tracker
                      :freshmeat
                      "https://freshmeat.io/"
                      "freshMeat is a Public torrent meta-search engine"
                      "en-US")})

(defn get-tracker [tracker]
  (get trackers tracker))

(defn parse-html
  [url]
  (try 
    (let [raw-html (get (client/get url) :body)]
      (enlive/html-snippet raw-html))
    (catch Exception e nil)))

(defn get-search-tracker [tracker-id query]
  (let [tracker (get-tracker (keyword tracker-id))]
    {(keyword tracker-id) (tracker/search tracker query)}))

(defn get-search-tracker-list [tracker-list query]
  (map #(get-search-tracker % query)
       tracker-list))
