(ns buccaneer.tracker.1337x
  (:require [buccaneer.tracker :refer :all]
            [buccaneer.core :refer :all]
            [net.cgrand.enlive-html :as enlive]
            [clj-http.client :as client]
            [clojure.string :as str]
            [uri.core :as uri])
  (:import (java.net URI URLEncoder URLDecoder)))

(def pages (agent {}))

(defmethod generate-search-link :1337x [tracker query]
  (uri/make (format "%ssearch/%s/" (:url tracker)
                    (uri/url-encode query))))

(defmethod get-page :1337x [tracker query number]
  (-> (format "%s%s/"
              (-> tracker
                  (generate-search-link query)
                  (.toString))
              number)
      (parse-html)))

(defn last-page
  [page]
  (-> page
      (enlive/select [:li.last :a])
      (first)
      :attrs
      :href))

(defn total-pages
  [url]
  (Integer/parseInt
   (last (str/split url #"/"))))

(defmethod search :1337x [tracker query]
  (when (not-empty @pages)
    (alter-var-root #'pages (constantly (agent {}))))

  (let [search-link (generate-search-link tracker query)
        first-page  (get-page tracker query 1)
        url   (last-page first-page)
        total-pages (and url (total-pages url))]
    (if total-pages
      (do (dotimes [n total-pages]
            (future
              (send pages assoc (inc n)
                    (-> (get-page tracker query (inc n))
                        (enlive/select [:td.coll-1 ])))
              (println (inc n))))
          (while (not= (count @pages) total-pages)
            (Thread/sleep 500))
          @pages)
      (not-empty
       (-> first-page
           (enlive/select [:td.coll-1 ]))))))

(defmethod get-torrent-info :1337x [tracker href]
  (let [torrent-link (str (:url tracker )
                          (str/replace-first href #"/" ""))
        torrent-page (parse-html torrent-link)
        seeds (-> torrent-page (enlive/select [:.seeds]) (first) :content (first))
        leeches (-> torrent-page (enlive/select [:.leeches]) (first) :content (first))
        magnet (-> torrent-page (enlive/select [:.clearfix :a ])
                   (nth 2 true) :attrs :href)
        files (count (-> torrent-page (enlive/select [:#files :li]) ))
        size (-> torrent-page (enlive/select [:.list :span ])
                 (nth 3 true) :content first)]
    (when magnet 
      {:magnet magnet
       :size size
       :files files
       :seeds seeds
       :leeches leeches})))
