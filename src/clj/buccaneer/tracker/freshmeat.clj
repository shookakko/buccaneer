(ns buccaneer.tracker.freshmeat
  (:require [buccaneer.tracker :refer :all]
            [buccaneer.core :refer :all]
            [clojure.string :as str]
            [net.cgrand.enlive-html :as enlive]
            [uri.core :as uri]))

(def pages (agent {}))

(defmethod generate-search-link :freshmeat [tracker query]
  (uri/make (format "%ss?q=%s" (:url tracker) (uri/url-encode query))))

(defmethod get-page :freshmeat [tracker query number]
  (-> (format "%s&skip=%s"
              (-> tracker
                  (generate-search-link query)
                  (.toString))
              (* number 100))
      (parse-html)))

(defn search-results
  [first-page]
  (-> first-page
      (enlive/select [:div.mt-2])
      (first)
      :content
      (first)))

(defn total-pages
  [search-results]
  (-> search-results
      (->>
       (re-find #"\d+"))
      (Integer/parseInt)
      (quot 100)))

(defn parse-html-result [html]
  (map #(let [href (-> %
                       :attrs
                       :href)
              name (-> %
                       :content
                       (first)
                       (str/trim))]
          {:href href
           :name name}) html))

(defmethod search :freshmeat [tracker query]
  (when (not-empty @pages)
    (alter-var-root #'pages (constantly (agent {}))))

  (let [search-link (generate-search-link tracker query)
        first-page (get-page tracker query 0)
        search-results (search-results first-page)
        total-pages (and search-results
                         (total-pages search-results))]
    (if (> total-pages 0)
      (do (dotimes [n (inc total-pages)]
            (future
              (send pages assoc n
                    (-> (get-page tracker query n)
                        (enlive/select [:tr.link :> :td :> :a])
                        (parse-html-result)
                        ))
              (println n)))
          (while (not= (count @pages) (inc total-pages))
            (Thread/sleep 500))
          @pages)
      (not-empty
       (-> first-page
           (enlive/select [:tr.link :> :td :> :a])
           (parse-html-result)
           )))))

(defmethod get-torrent-info :freshmeat [tracker href]
  (when (re-find #"/t/" href)
    (let [torrent-link (str (:url tracker) href)
          torrent-page (parse-html torrent-link)]
      (when torrent-page
        (let [magnet (-> torrent-page
                         (enlive/select [:div.col-12 :> :a.btn])
                         (first)
                         :attrs
                         :href
                         (uri/url-decode))
              size (-> torrent-page
                       (enlive/select [:div.col-12 :p.badge])
                       (first)
                       :content
                       (first)
                       (->> (re-find #"\d+.?\d*\s\w+")))
              files (-> torrent-page
                        (enlive/select [:div.col-12 :> :ul :> :li :> :a :> :small])
                        (first)
                        :content
                        (first)
                        (Integer/parseInt))]
          {:magnet magnet
           :size size
           :files files
           :seeds nil
           :leechers nil})))))
