(ns buccaneer.api
  (:require [buccaneer.core :refer :all]
            [liberator.core :refer [resource defresource]]
            [hiccup.page :refer [include-js include-css html5]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.resource :refer [wrap-resource]]
            [compojure.core :refer [context defroutes ANY GET POST]]
            [config.core :refer [env]]
            [cheshire.core :as cheshire]))

(defn make-query [json]
  (let [name (get json :name)
        tracker-list (get json :tracker-list)]
    (format "Looking for %s in %s..." name tracker-list)))

(defn parse-json [body key]
  (try
    (if-let [json (cheshire/parse-string body true)]
      (if (and (= (count json) 2)
               (get json :name)
               (get json :tracker-list))
        [false {key json}]
        {:message "wrong json"})
      {:message "no body"})
    (catch Exception e
      {:message "bad json"})))

(defresource get-search
  :available-media-types ["application/json"]
  :allowed-methods [:post]
  :malformed? #(parse-json (slurp (get-in % [:request :body])) ::json) 
  :post! #(let [d (make-query (get % ::json))] {::data d})
  :handle-created ::json
  :handle-malformed #(format "{\"error\": \"%s\"}" (get % :message)))

(defresource find-trackers
  :available-media-types ["application/json"]
  :allowed-methods [:get]
  :handle-ok (fn [ctx]
               (cheshire/encode trackers)))

(def mount-target
  [:div#app
   [:h2 "Welcome to template"]
   [:p "please wait while Figwheel/shadow-cljs is waking up ..."]
   [:p "(Check the js console for hints if nothing exciting happens.)"]])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]
   (include-css (if (env :dev) "/css/site.css" "/css/site.min.css"))])

(defn loading-page []
  (html5
   (head)
   [:body {:class "body-container"}
    mount-target
    (include-js "/js/app.js")]))

(defresource page
  :available-media-types ["text/html"]
  :handle-ok (fn [ctx] (loading-page)))

(defroutes handler
  (GET "/" [] page)
  (GET "/items" [] page)
  (GET "/items/:item-id" [item] page)
  (GET "/about" page)
  (context "/api" []
           (POST "/search" [] get-search)
           (GET "/available-trackers" [] find-trackers)))

                                        ; ENDPOINTS
;; /magnet-information
;; /torrent

(def app
  (-> handler
      (wrap-resource "public")
      wrap-params))
