(ns buccaneer.tracker
  (:require [net.cgrand.enlive-html :as enlive]))

(defstruct tracker
  :id
  :url
  :description
  :language)

(defmulti generate-search-link (fn [tracker query] (:id tracker)))
(defmulti search (fn [tracker query] (:id tracker)))
(defmulti get-torrent-info (fn [tracker torrent] (:id tracker)))
(defmulti get-page (fn [tracker url number] (:id tracker)))

